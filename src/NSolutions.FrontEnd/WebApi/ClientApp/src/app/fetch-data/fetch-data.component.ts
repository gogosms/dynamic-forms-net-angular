import { Component, Inject } from '@angular/core';
import { DisplayTemplate } from '../shared/dynamic-forms-module/displays/display-template.enum';
import { ComponentMetadata } from '../shared/dynamic-forms-module/models/component-metadata.model';


@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  properties: ComponentMetadata[] = [];
  constructor() {
      this.properties= [
        {
          metadata: {
            display: DisplayTemplate.string,
            id: 'Test1',
            label: 'Label => Test 01',
            

          },
          value: 'test01'
        },
        {
          metadata: {
            display:  DisplayTemplate.string,
            id: 'Test2',
            label: 'Label => Test 02'
          },
          value: 'test02'
        },
        {
          metadata: {
            display: DisplayTemplate.bool,
            id: 'Test3',
            label: 'Label => Test 02'
          },
          value: true
        },
        {
          metadata: {
            display: DisplayTemplate.bool,
            id: 'Test4',
            label: 'Label => Test 02'
          },
          value: false
        },
        {
          metadata: {
            display: DisplayTemplate.checkbox,
            id: 'Test5',
            label: 'Label => Test 02'
          },
          value: true
        },
        {
          metadata: {
            display: DisplayTemplate.checkbox,
            id: 'Test6',
            label: 'Label => Test 02'
          },
          value: false
        }
      ];
  }
}

