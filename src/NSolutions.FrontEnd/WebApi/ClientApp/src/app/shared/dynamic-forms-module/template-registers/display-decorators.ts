if(window['global'] === undefined)
{ 
    (window as any).global = window 
}

import { DynamicForms } from "../dynamic-forms.const";
import { TemplateConfig } from "./template-config.models";

export const reflect = (window as any).global['Reflect'];

export function display(config: { name: string }) {
    return (target: Object) => {
        const templateConfig: TemplateConfig = { name: config.name };
        reflect.defineMetadata(DynamicForms.templateName, templateConfig, target);
    };
}

