import { TemplateConfig } from "./template-config.models";
import { Type } from '@angular/core';


export class TemplateRegistryItem {
    constructor(public component: Type<any>, public config: TemplateConfig) { }
}
