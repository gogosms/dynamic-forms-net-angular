import { Component, OnInit } from '@angular/core';
import { display } from '../../template-registers/display-decorators';
import { DisplayComponentBase } from '../display-component-base';
import { DisplayTemplate } from '../display-template.enum';

@Component({
  selector: 'app-boolean-display',
  templateUrl: './boolean-display.component.html',
  styleUrls: ['./boolean-display.component.css']
})

@display({ name: DisplayTemplate.bool })
export class BooleanDisplayComponent extends DisplayComponentBase implements OnInit {

  constructor() { 
    super();
  }

  ngOnInit() {
  }

}
