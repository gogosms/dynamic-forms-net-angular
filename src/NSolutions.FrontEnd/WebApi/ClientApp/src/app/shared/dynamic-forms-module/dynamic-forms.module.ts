import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DisplaysRegistryService } from './displays/displays-registry.service';
import { StringDisplayComponent } from './displays/string-display/string-display.component';
import { DynamicDisplayDirective } from './displays/dynamic-display.directive';
import { BooleanDisplayComponent } from './displays/boolean-display/boolean-display.component';
import { BootstrappingDisplay } from './displays/bootstrapping-displays.install';
import { CheckboxDisplayComponent } from './displays/checkbox-display/checkbox-display.component';

@NgModule({
  declarations: [ StringDisplayComponent, 
                  DynamicDisplayDirective, BooleanDisplayComponent, CheckboxDisplayComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
   ],
  
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    DynamicDisplayDirective
    
  ],
  
  entryComponents: [
      StringDisplayComponent, 
      BooleanDisplayComponent, 
      CheckboxDisplayComponent],
  
  providers: [DisplaysRegistryService]
})
export class DynamicFormsModule {
    constructor(displaysRegistry: DisplaysRegistryService) {
      BootstrappingDisplay.Install(displaysRegistry);
    }

 }

