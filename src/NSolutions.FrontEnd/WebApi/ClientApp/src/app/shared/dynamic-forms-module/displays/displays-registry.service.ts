import { Injectable, Type } from "@angular/core";
import { TemplateBaseRegistryServiceBase } from "../template-registers/template-base-registry-service-base";


@Injectable()
export class DisplaysRegistryService extends TemplateBaseRegistryServiceBase {
    
    constructor() {
        super();
    }

    public getDisplayTemplate(name: any): Type<any> {
        return this.getTemplate(name);
    }
    
}
