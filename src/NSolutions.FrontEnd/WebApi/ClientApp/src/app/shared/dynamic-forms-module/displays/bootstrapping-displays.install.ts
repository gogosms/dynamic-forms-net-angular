import { BooleanDisplayComponent } from "./boolean-display/boolean-display.component";
import { CheckboxDisplayComponent } from "./checkbox-display/checkbox-display.component";
import { DisplaysRegistryService } from "./displays-registry.service";
import { StringDisplayComponent } from "./string-display/string-display.component";

export class BootstrappingDisplay {
    
    public static Install(registry: DisplaysRegistryService) {

        registry.register(StringDisplayComponent);
        registry.register(BooleanDisplayComponent);
        registry.register(CheckboxDisplayComponent);
    }
}