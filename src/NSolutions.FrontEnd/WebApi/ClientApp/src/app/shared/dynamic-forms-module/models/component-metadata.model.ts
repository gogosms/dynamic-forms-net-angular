import { DisplayTemplate } from "../displays/display-template.enum";

export interface ComponentMetadata {
    metadata: {
                id: string;
                label: string;
                display: DisplayTemplate;
              },
    value: any 
               // Aca deberia ser propia del componente la implementacion del valor
               // DotNet => Usar una interface generica y otra no generica, con el mismo nombre!
}

