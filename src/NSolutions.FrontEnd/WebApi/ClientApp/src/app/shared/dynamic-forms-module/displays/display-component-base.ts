import { Input } from '@angular/core';
import { ComponentMetadata } from '../models/component-metadata.model';

export abstract class DisplayComponentBase {
    
    @Input() metadata: ComponentMetadata;
    @Input() value: any;

    
}


