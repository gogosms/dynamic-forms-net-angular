import { Component, OnInit } from '@angular/core';
import { display } from '../../template-registers/display-decorators';
import { DisplayComponentBase } from '../display-component-base';
import { DisplayTemplate } from '../display-template.enum';

@Component({
  selector: 'app-checkbox-display',
  templateUrl: './checkbox-display.component.html',
  styleUrls: ['./checkbox-display.component.css']
})
@display({ name: DisplayTemplate.checkbox })

export class CheckboxDisplayComponent extends DisplayComponentBase implements OnInit {

  constructor() {
    super();
   }

  ngOnInit() {
  }

}
