import { TemplateConfig } from "./template-config.models";
import { Type } from '@angular/core';
import { reflect } from "./display-decorators";
import { DynamicForms } from "../dynamic-forms.const";
import { TemplateRegistryItem } from "./template-registry-item.models";


export abstract class TemplateBaseRegistryServiceBase {
    private templates: TemplateRegistryItem[] = [];
    private registeredComponents: Type<any>[] = [];

    constructor() { }

    public register(component: Type<any>, templateConfig?: TemplateConfig) {
        if (this.registeredComponents.indexOf(component) >= 0) {
            throw new Error(`Component '${component.name}' was already registered.`);
        }
        if (!templateConfig) {
            templateConfig = reflect.getMetadata(DynamicForms.templateName, component);
            if (!templateConfig) {
                throw new Error(`Component '${component.name}' is missing its metadata.`);
            }
        }
        this.templates.push(new TemplateRegistryItem(component, templateConfig));
        this.registeredComponents.push(component);
    }
    
    public getTemplate(name: any): Type<any> {
        const editor = this.templates.find((e) => e.config.name === name);
        return editor ? editor.component : null;
    }
   
}


