import { ComponentFactoryResolver, ComponentRef, Directive, Input, ViewContainerRef } from '@angular/core';
import { DisplayComponentBase } from './display-component-base';
import { DisplaysRegistryService } from './displays-registry.service';

@Directive({
  selector: '[appDynamicDisplayHost]'
})
export class DynamicDisplayDirective {

    @Input() metadata: any // PropertyBagMetadataItem;
    @Input() value: any;
    
    component: ComponentRef<DisplayComponentBase>;
    @Input() parent: any;
    constructor(
        private container: ViewContainerRef,
        private resolver: ComponentFactoryResolver,
        private displayTemplatesRegistry: DisplaysRegistryService) {}

    ngOnInit() {
      
      const displayComponent = this.displayTemplatesRegistry.getDisplayTemplate(this.metadata.display);
      
      if (!displayComponent) {
          throw new Error(`Trying to use an unsupported type (${this.metadata.display}).`);
      }
      
      const component = this.resolver.resolveComponentFactory<DisplayComponentBase>(displayComponent);
      this.component = this.container.createComponent(component);
      this.component.instance.metadata = this.metadata;
      this.component.instance.value = this.value;
    }

    

}
