import { Component, OnInit } from '@angular/core';
import { display } from '../../template-registers/display-decorators';
import { DisplayComponentBase } from '../display-component-base';
import { DisplayTemplate } from '../display-template.enum';

@Component({
  selector: 'app-string-display',
  templateUrl: './string-display.component.html',
  styleUrls: ['./string-display.component.css']
})

@display({ name: DisplayTemplate.string })
export class StringDisplayComponent extends DisplayComponentBase implements OnInit {

  constructor() { 
    super();
  }

  ngOnInit() {
  }

}
